import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router, RouterOutlet } from '@angular/router';
import { BreadcrumbService } from "app/core/services/breadcrumb/breadcrumb.service";
import { Breadcrumb } from "app/core/services/breadcrumb/breadcrumb.type";
import { filter } from "rxjs";

@Component({
    selector   : 'app-root',
    templateUrl: './app.component.html',
    styleUrls  : ['./app.component.scss'],
    standalone : true,
    imports    : [RouterOutlet],
})
export class AppComponent implements OnInit
{
    /**
     * Constructor
     */
    constructor(private router: Router, private activatedRoute: ActivatedRoute, private _breadcrumbService: BreadcrumbService)
    {
    }
    
    ngOnInit(): void {
        this.router.events
            .pipe(filter(event => event instanceof NavigationEnd))
            .subscribe(() => {
                this._breadcrumbService.breadcrumbs = this.generateBreadcrumbs(this.activatedRoute.root);
            });
    }

    generateBreadcrumbs(route: ActivatedRoute, parentUrl = '', breadcrumbs: Breadcrumb[] = []): Breadcrumb[] {
    
        const { snapshot, children } = route;
        const { data, url } = snapshot;

        if (snapshot.routeConfig && snapshot.routeConfig.loadChildren && typeof snapshot.routeConfig.loadChildren === 'function')
        {
            if (children.length > 0) {
                for (const childRoute of children) {
                    this.generateBreadcrumbs(childRoute, parentUrl + '/' + url.map(segment => segment.path).join('/'), breadcrumbs);
                }
            }
            return breadcrumbs;
        }
            
        if (data && data.breadcrumb) {
            let breadcrumbLabel = data.breadcrumb;
            if (data.breadcrumb.includes(':id')) {
              const id = url[url.length - 1].path;
              breadcrumbLabel = breadcrumbLabel.replace(':id', id);
            }
        
            const breadcrumb: Breadcrumb = {
              label: breadcrumbLabel,
              url: parentUrl + '/' + url.map(segment => segment.path).join('/')
            };


            breadcrumbs.push(breadcrumb);
        }

        if (data && data.title) {
            this._breadcrumbService.pageTitle = data.title;
        }

        if (children.length > 0) {
            for (const childRoute of children) {
                this.generateBreadcrumbs(childRoute, parentUrl + '/' + url.map(segment => segment.path).join('/'), breadcrumbs);
            }
        }
        return breadcrumbs;
    }
}
