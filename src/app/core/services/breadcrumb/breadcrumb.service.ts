import { Injectable } from '@angular/core';
import { Breadcrumb } from "app/core/services/breadcrumb/breadcrumb.type";
import { BehaviorSubject, Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class BreadcrumbService {

  private _pageTitle: BehaviorSubject<string> = new BehaviorSubject<string>('');
  private _breadcrumbs: BehaviorSubject<Breadcrumb[]> = new BehaviorSubject<Breadcrumb[]>([]);

  set breadcrumbs(breadcrumbs: Breadcrumb[]) {
    this._breadcrumbs.next(breadcrumbs);
  }

  get breadcrumbs$(): Observable<Breadcrumb[]> {
    return this._breadcrumbs.asObservable();
  }

  set pageTitle(pageTitle: string) {
    this._pageTitle.next(pageTitle);
  }

  get pageTitle$(): Observable<string> {
    return this._pageTitle.asObservable();
  }
  
  constructor() { }
}
