import { NgFor, NgIf } from "@angular/common";
import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { RouterModule } from "@angular/router";
import { BreadcrumbService } from "app/core/services/breadcrumb/breadcrumb.service";
import { Breadcrumb } from "app/core/services/breadcrumb/breadcrumb.type";
import { Observable, Subject, takeUntil } from "rxjs";

@Component({
    selector: 'breadcrumb',
    templateUrl: './breadcrumb.component.html',
    encapsulation: ViewEncapsulation.None,
    exportAs: 'breadcrumb',
    standalone: true,
    imports: [ NgFor, NgIf, RouterModule ]
})
export class BreadcrumbComponent implements OnInit, OnDestroy {

    breadcrumbs: Breadcrumb[] = [];
    pageTitle$: Observable<string>;

    private _unsubscribeAll: Subject<any> = new Subject<any>();

    constructor(private _breadcrumbService: BreadcrumbService) { }

    ngOnInit(): void {
        // Subscribe to breadcrumbs
        this._breadcrumbService.breadcrumbs$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((breadcrumbs: Breadcrumb[]) => {
                this.breadcrumbs = breadcrumbs;
            });

        // Subscribe to page title
        this.pageTitle$ = this._breadcrumbService.pageTitle$;
    }

    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    }
}
