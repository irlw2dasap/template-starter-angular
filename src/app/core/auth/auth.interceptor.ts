import { HttpErrorResponse, HttpEvent, HttpHandler, HttpHandlerFn, HttpRequest } from '@angular/common/http';
import { inject } from '@angular/core';
import { Router } from "@angular/router";
import { AuthService } from 'app/core/auth/auth.service';
import { AuthUtils } from 'app/core/auth/auth.utils';
import { catchError, Observable, switchMap, throwError } from 'rxjs';


const handleUnauthenticatedUser = (router: Router, authService: AuthService): void => {
    authService.signOut().subscribe(() => {
        router.navigate(['/sign-in']);
    });
};

const handleAuthenticatedUser = (authService: AuthService): void => {
    authService.signOut().subscribe(() => {
        location.reload();
    });
};

const handleUnauthorizedError = (req: HttpRequest<any>, next: HttpHandlerFn, router: Router, authService: AuthService): Observable<HttpEvent<any>> => {

    if (!authService.isAuthenticated()) {
        handleUnauthenticatedUser(router, authService);
        return throwError({ status: 401, message: 'Unauthorized' });
    }

    return authService.refreshAccessToken().pipe(
        switchMap(() => {
            const newReq = req.clone({
                headers: req.headers.set('Authorization', 'Bearer ' + authService.accessToken)
            });
            return next(newReq);
        }),
        catchError((error) => {
            if (error instanceof HttpErrorResponse && error.status === 401) {
                handleUnauthenticatedUser(router, authService);
                return throwError({ status: 401, message: 'Unauthorized' });
            }
            return throwError(error);
        })
    );
};

const handleBadRequestError = (req: HttpRequest<any>, next: HttpHandlerFn, router: Router, authService: AuthService): Observable<HttpEvent<any>> => {

    if (!authService.isAuthenticated()) {
        handleUnauthenticatedUser(router, authService);
        return throwError({ status: 401, message: 'Unauthorized' });
    }

    return authService.refreshAccessToken().pipe(
        switchMap(() => {
            const newReq = req.clone({
                headers: req.headers.set('Authorization', 'Bearer ' + authService.accessToken)
            });
            return next(newReq);
        }),
        catchError((error) => {
            if (error instanceof HttpErrorResponse && error.status === 400) {
                handleUnauthenticatedUser(router, authService);
                return throwError({ status: 400, message: 'Bad Request' });
            }
            return throwError(error);
        })
    );
};

/**
 * Intercept
 *
 * @param req
 * @param next
 */
export const authInterceptor = (req: HttpRequest<unknown>, next: HttpHandlerFn): Observable<HttpEvent<unknown>> => {
    const authService = inject(AuthService);
    const router = inject(Router);

    // Clone the request object
    let newReq = req.clone();

    // Request
    //
    // If the access token didn't expire, add the Authorization header.
    // We won't add the Authorization header if the access token expired.
    // This will force the server to return a "401 Unauthorized" response
    // for the protected API routes which our response interceptor will
    // catch and delete the access token from the local storage while logging
    // the user out from the app.
    if (authService.accessToken && !AuthUtils.isTokenExpired(authService.accessToken)) {
        newReq = req.clone({
            headers: req.headers.set('Authorization', 'Bearer ' + authService.accessToken),
        });
    }

    // Response
    return next(newReq).pipe(
        catchError((error) => {

            if (error instanceof HttpErrorResponse) {
                if (error.status === 403) {
                    if (!authService.isAuthenticated()) {
                        handleUnauthenticatedUser(router, authService);
                    } else {
                        handleAuthenticatedUser(authService);
                    }
                } else if (error.status === 401) {
                    return handleUnauthorizedError(req, next, router, authService);
                } else if (error.status === 400) {
                    return handleBadRequestError(req, next, router, authService);
                }
            }

            return throwError(error);
        }),
    );


};

