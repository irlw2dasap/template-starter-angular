export interface RequestResponse<T> {
    data: T;
    message: string;
    pagination?: PaginationResponse;
    status: 'success' | 'fail' | 'error';
}

export interface PaginationResponse {
    length: number;
    size: number;
    page: number;
    lastPage: number;
    startIndex: number;
    endIndex: number;
}

export interface NextPage {
    next: string;
}

export interface PaginationSocialPageResponse<T> {
    data: T;
    paging: NextPage;
}