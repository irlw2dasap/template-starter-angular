export interface FbSocialPage {
    id: string;
    name: string;
    username: string;
    picture: string;
    fan_count: string;
    link?: string;
    about?: string;
}