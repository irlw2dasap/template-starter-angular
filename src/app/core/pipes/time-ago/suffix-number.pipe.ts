import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'suffixNumber',
    pure: false,
    standalone: true,
})
export class SuffixNumberPipe implements PipeTransform {

    constructor() {
    }

    transform(value: number): string {
        if (!value)
        {
            return '0';
        }
        if (value >= 1000000) {
            return (value / 1000000).toFixed(1) + 'M';
        } else if (value >= 1000) {
            return (value / 1000).toFixed(1) + 'K';
        } else {
            return value.toString();
        }
    }
}
