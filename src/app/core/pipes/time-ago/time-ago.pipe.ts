import { Pipe, PipeTransform } from '@angular/core';

/**
 * Finds an object from given source using the given key - value pairs
 */
@Pipe({
    name: 'timeAgo',
    pure: false,
    standalone: true,
})
export class TimeAgoPipe implements PipeTransform {
    /**
     * Constructor
     */
    constructor() {
    }

    /**
     * Transform
     *
     * @param value A string or an array of strings to find from source
     * @param key Key of the object property to look for
     * @param source Array of objects to find from
     */
    transform(date: any, args?: any): any {
        let seconds = Math.floor((+new Date() - +new Date(date)) / 1000);

        if (seconds < 29) // less than 30 seconds ago will show as 'Just now'
            return 'เมื่อครู่นี้';

        const intervals = {
            'ปี': 31536000,
            'เดือน': 2592000,
            'สัปดาห์': 604800,
            'วัน': 86400,
            'ชั่วโมง': 3600,
            'นาที': 60,
            'วินาที': 1
        };

        let counter;
        for (const i in intervals) {
            counter = Math.floor(seconds / intervals[i]);
            if (counter > 0)
                if (counter === 1) {
                    return counter + ' ' + i + 'ที่ผ่านมา';   // singular (1 day ago)
                } else {
                    return counter + ' ' + i + 'ที่ผ่านมา';   // plural (2 days ago)
                }
        }
    }
}
