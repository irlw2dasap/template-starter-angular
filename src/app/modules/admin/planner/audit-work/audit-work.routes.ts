import { Routes } from '@angular/router';
import { PlannerAuditWorkComponent } from "app/modules/admin/planner/audit-work/audit-work.component";

export default [
    {
        path     : '',
        data: {
            breadcrumb: 'การปฏิบัติงานตรวจสอบ'
        },
        children: [
            {
                path     : 'test',
                data: {
                    breadcrumb: 'ทดสอบ'
                },
                component: PlannerAuditWorkComponent,
            }
        ]
    },
] as Routes;
