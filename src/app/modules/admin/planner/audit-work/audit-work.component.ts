import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";

@Component({
    selector     : 'app-planner-audit-work',
    standalone   : true,
    templateUrl  : './audit-work.component.html',
    encapsulation: ViewEncapsulation.None,
    imports: [ ]
})
export class PlannerAuditWorkComponent implements OnInit
{
    /**
     * Constructor
     */


    constructor(
        private _fb: FormBuilder
    )
    {
    }

    ngOnInit(): void {

    }

}
