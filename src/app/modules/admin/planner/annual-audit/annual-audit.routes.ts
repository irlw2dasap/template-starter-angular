import { Routes } from '@angular/router';
import { PlannerAnnualAuditComponent } from 'app/modules/admin/planner/annual-audit/annual-audit.component';

export default [
    {
        path     : '',
        data: {
            breadcrumb: 'การตรวจสอบประจำปี'
        },
        component: PlannerAnnualAuditComponent
    },
] as Routes;
