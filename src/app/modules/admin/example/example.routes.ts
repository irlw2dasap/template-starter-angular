import { Routes } from '@angular/router';
import { ExampleComponent } from 'app/modules/admin/example/example.component';

export default [
    {
        path     : '',
        data: {
            breadcrumb: 'ทดสอบ 1'
        },
        component: ExampleComponent,
    },
] as Routes;
