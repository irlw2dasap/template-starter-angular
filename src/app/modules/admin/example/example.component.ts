import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";

@Component({
    selector     : 'example',
    standalone   : true,
    templateUrl  : './example.component.html',
    encapsulation: ViewEncapsulation.None,
    imports: [ ]
})
export class ExampleComponent implements OnInit
{
    /**
     * Constructor
     */


    constructor(
        private _fb: FormBuilder
    )
    {
    }

    ngOnInit(): void {

    }

}
