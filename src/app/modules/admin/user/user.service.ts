import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { PaginationResponse, RequestResponse } from "app/core/models/response.types";
import { User } from "app/modules/admin/user/user.types";
import { environment } from "environments/environment";
import { BehaviorSubject, Observable, map, of, switchMap, take, tap, throwError } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class UserService {


  private _users: BehaviorSubject<User[]> = new BehaviorSubject(null);
  private _user: BehaviorSubject<User | null> = new BehaviorSubject(null);
  private _pagination: BehaviorSubject<PaginationResponse> = new BehaviorSubject(null);

  constructor(private _httpClient: HttpClient) { }


  // -----------------------------------------------------------------------------------------------------
  // @ Accessors
  // -----------------------------------------------------------------------------------------------------


  /**
   * Getter for user
   */
  get user$(): Observable<User> {
    return this._user.asObservable();
  }

  /**
   * Getter for users
   */
  get users$(): Observable<User[]> {
    return this._users.asObservable();
  }

  /**
   * Getter for pagination
   */
  get pagination$(): Observable<PaginationResponse> {
    return this._pagination.asObservable();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  getData(): Observable<User> {
    return this._httpClient.get(environment.apiUrl + '/api/v1/user').pipe(
      tap((response: any) => {
        this._users.next(response);
      }),
    );
  }

  searchData(q: string = '', page: number = 1, size: number = 10, sort: string = 'createdAt', order: 'asc' | 'desc' | '' = 'desc', filter: any = "[]"): Observable<RequestResponse<User[]>> {
    return this._httpClient.get(environment.apiUrl + '/api/v1/user/search', {
      params: {
        q: q,
        page: page.toString(),
        size: size.toString(),
        sort: sort,
        order: order,
        filter: filter
      }
    }).pipe(
      tap((response: RequestResponse<User[]>) => {
        this._users.next(response.data);
        this._pagination.next(response.pagination);
      })
    );
  }

  getUserById(id: number): Observable<User> {
    return this._users.pipe(
      take(1),
      map((users) => {
        // Find the user
        const user = users.find(item => item.userId === id) || null;

        // Update the user
        this._user.next(user);

        // Return the user
        return user;
      }),
      switchMap((user) => {
        if (!user) {
          return throwError('Could not found user with id of ' + id + '!');
        }

        return of(user);
      })
    );
  }

}
