import { DatePipe, NgClass, NgIf } from "@angular/common";
import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatSort, MatSortModule } from "@angular/material/sort";
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { UserService } from "app/modules/admin/user/user.service";
import { Subject, debounceTime, map, merge, switchMap, takeUntil } from "rxjs";
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { PaginationResponse } from "app/core/models/response.types";
import { MatIconModule } from "@angular/material/icon";
import { MatButtonModule } from "@angular/material/button";
import { MatFormFieldModule } from "@angular/material/form-field";
import { FormsModule, ReactiveFormsModule, UntypedFormControl } from "@angular/forms";
import { MatProgressBarModule } from "@angular/material/progress-bar";
import { MatInputModule } from "@angular/material/input";
import { RouterModule } from "@angular/router";
import { TimeAgoPipe } from "app/core/pipes/time-ago";
import { fuseAnimations } from "@fuse/animations";

@Component({
    selector: 'app-user',
    standalone: true,
    templateUrl: './user.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations,
    imports: [NgIf, MatTableModule, RouterModule, MatSortModule, MatPaginatorModule, MatFormFieldModule, MatInputModule, FormsModule, MatProgressBarModule, ReactiveFormsModule, MatIconModule, MatButtonModule, NgClass, DatePipe, TimeAgoPipe]
})
export class UserComponent implements OnInit, AfterViewInit, OnDestroy {

    @ViewChild(MatPaginator) private _paginator: MatPaginator;
    @ViewChild(MatSort) private _sort: MatSort;

    searchInputControl: UntypedFormControl = new UntypedFormControl();

    isLoading: boolean = false;

    data: any;
    pagination: PaginationResponse;

    userDataSource: MatTableDataSource<any> = new MatTableDataSource();
    userTableColumns: string[] = ['userId', 'username', 'firstName', 'createdBy', 'createdAt', 'lastLogin', 'actions'];
    private _unsubscribeAll: Subject<any> = new Subject<any>();


    /**
     * Constructor
     */
    constructor(
        private _changeDetectorRef: ChangeDetectorRef,
        private _userService: UserService
    ) {
    }

    /**
     * On init
     */
    ngOnInit(): void {
        // Get the data
        this._userService.users$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((data) => {
                this.data = data;
                this.userDataSource.data = data;

                // Mark for check
                this._changeDetectorRef.markForCheck();
            });

        // Get the pagination
        this._userService.pagination$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((pagination: PaginationResponse) => {
                // Update the pagination
                this.pagination = pagination;

                // Mark for check
                this._changeDetectorRef.markForCheck();
            });

        // Subscribe to search input field value changes
        this.searchInputControl.valueChanges
            .pipe(
                takeUntil(this._unsubscribeAll),
                debounceTime(300),
                switchMap((query) => {
                    this.isLoading = true;
                    return this._userService.searchData(query, this.pagination?.page || 1, this.pagination.size || 10, this._sort.active, this._sort.direction);
                }),
                map(() => {
                    this.isLoading = false;
                }),
            )
            .subscribe();
    }

    /**
     * After view init
     */
    ngAfterViewInit(): void {
        setTimeout(() => {
            if (this._sort) {
                // Set the initial sort
                this._sort.sort({
                    id: 'createdAt',
                    start: 'desc',
                    disableClear: true,
                });

                // Mark for check
                this._changeDetectorRef.markForCheck();

                // If the user changes the sort order...
                this._sort.sortChange
                    .pipe(takeUntil(this._unsubscribeAll))
                    .subscribe(() => {
                        if (this.pagination) {
                            this.pagination.page = 0;
                        }
                    });

                merge(this._sort.sortChange).pipe(
                    switchMap(() => {
                        this.isLoading = true;
                        return this._userService.searchData(this.searchInputControl.value || "", (this.pagination.page + 1) || 1, this.pagination.size, this._sort.active, this._sort.direction);
                    }),
                    map(() => {
                        this.isLoading = false;
                    }),
                ).subscribe();
            }
        });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    }

    onPageChange(event: any): void {

        this.pagination.page = event.pageIndex + 1;
        this.pagination.size = event.pageSize;

        this._userService.searchData(this.searchInputControl.value || "", this.pagination?.page || 1, this.pagination?.size || 10, this._sort?.active, this._sort?.direction)
            .pipe(
                takeUntil(this._unsubscribeAll),
                map(() => {
                    this.isLoading = false;
                })
            )
            .subscribe();

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Track by function for ngFor loops
     *
     * @param index
     * @param item
     */
    trackByFn(index: number, item: any): any {
        return item.userId || index;
    }
}
