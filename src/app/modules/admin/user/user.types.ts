

export interface User {

    userId: number;
    username: string;
    password?: string;
    firstName: string;
    lastName: string;
    email: string;
    role: string;

    createdAt: Date;
    createdBy: string;

    updatedAt?: Date;
    updatedBy?: string;

    useStatus: 'A' | 'I';
    recordStatus: 'A' | 'I';

}