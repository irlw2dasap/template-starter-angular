import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'user-inspect',
  standalone: true,
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class UserInspectComponent {

}
