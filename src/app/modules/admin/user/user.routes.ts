import { inject } from "@angular/core";
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot, Routes } from '@angular/router';
import { UserInspectComponent } from "app/modules/admin/user/details/details.component";
import { UserComponent } from "app/modules/admin/user/user.component";
import { UserService } from "app/modules/admin/user/user.service";
import { catchError, throwError } from "rxjs";

/**
 * User resolver
 *
 * @param route
 * @param state
 */
const userResolver = (route: ActivatedRouteSnapshot, state: RouterStateSnapshot) => {
    const userService = inject(UserService);
    const router = inject(Router);

    return userService.getUserById(parseInt(route.paramMap.get('id')))
        .pipe(
            // Error here means the requested contact is not available
            catchError((error) => {
                // Log the error
                console.error(error);

                // Get the parent url
                const parentUrl = state.url.split('/').slice(0, -1).join('/');

                // Navigate to there
                router.navigateByUrl(parentUrl);

                // Throw an error
                return throwError(error);
            }),
        );
};

export default [
    {
        path: '',
        component: UserComponent,
        data: {
            breadcrumb: 'ระบบที่ 2'
        },
        resolve: {
            //data: () => inject(UserService).searchData()
        }
    },
    {
        path: 'new',
        data: {
            breadcrumb: 'สร้างระบบที่ 2'
        },
        component: UserInspectComponent
    },
    {
        path: ':id',
        data: {
            breadcrumb: 'แก้ไขระบบที่ 2 (#:id)',
            title: 'แก้ไขข้อมูลบุคลากร'
        },
        component: UserInspectComponent,
        resolve: {
            user: userResolver
        }
    },
] as Routes;
