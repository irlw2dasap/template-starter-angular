import { Routes } from '@angular/router';
import { SearchComponent } from "app/modules/admin/search/search.component";

export default [
    {
        path     : '',
        data: {
            breadcrumb: 'ระบบสืบค้นข้อมูล'
        },
        component: SearchComponent,
    },
] as Routes;
