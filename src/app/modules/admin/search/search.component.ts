import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";

@Component({
    selector     : 'app-search',
    standalone   : true,
    templateUrl  : './search.component.html',
    encapsulation: ViewEncapsulation.None,
    imports: [ ]
})
export class SearchComponent implements OnInit
{
    /**
     * Constructor
     */


    constructor(
        private _fb: FormBuilder
    )
    {
    }

    ngOnInit(): void {

    }

}
