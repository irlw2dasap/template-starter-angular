/* eslint-disable */
import { FuseNavigationItem } from '@fuse/components/navigation';

export const defaultNavigation: FuseNavigationItem[] = [
    {
        id: 'system',
        type: 'group',
        title: 'ระบบงาน',
        icon: 'heroicons_outline:rectangle-group',
        children: [
            {
                id: 'system.planner',
                title: 'การวางแผน',
                type: 'collapsable',
                icon: 'heroicons_outline:plus-circle',
                children: [
                    {
                        id: 'system.planner.annual_audit',
                        title: 'การตรวจสอบประจำปี',
                        type: 'basic',
                        link: '/apps/planner/annual-audit'
                    },
                    {
                        id: 'system.planner.audit_work',
                        title: 'การปฏิบัติงานตรวจสอบ',
                        type: 'basic',
                        link: '/apps/planner/audit-work'
                    }
                ]
            },
            {
                id: 'system.search_engine',
                title: 'ระบบสืบค้นข้อมูล',
                type: 'basic',
                icon: 'heroicons_outline:plus-circle',
                link: '/apps/search'
            }
        ]
    }/* ,
    {
        id: 'settings',
        type: 'group',
        title: 'การตั้งค่า',
        icon: 'heroicons_outline:cog-8-tooth',
        children: [
            {
                id: 'settings.first',
                title: 'ทดสอบ 1',
                type: 'basic',
                icon: 'heroicons_outline:tag',
                link: '/example'
            },
            {
                id: 'settings.second',
                title: 'ทดสอบ 2',
                type: 'basic',
                icon: 'heroicons_outline:stop',
                link: '/test/b'
            }
        ]
    } */
];

export const compactNavigation: FuseNavigationItem[] = [
    {
        id: 'system',
        type: 'group',
        title: 'ระบบงาน',
        children: []
    }/* ,
    {
        id: 'settings',
        type: 'group',
        title: 'การตั้งค่า',
        children: []
    } */
];
export const futuristicNavigation: FuseNavigationItem[] = [
    {
        id: 'system',
        type: 'group',
        title: 'ระบบงาน',
        children: []
    }/* ,
    {
        id: 'settings',
        type: 'group',
        title: 'การตั้งค่า',
        children: []
    } */
];
export const horizontalNavigation: FuseNavigationItem[] = [
    {
        id: 'system',
        type: 'group',
        title: 'ระบบงาน',
        children: []
    }/* ,
    {
        id: 'settings',
        type: 'group',
        title: 'การตั้งค่า',
        children: []
    } */
];